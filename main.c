#include <Timers.h>
const int MOISTURE_SENSOR_ANALOG_PIN = 0;
const int PUMP_DIGITAL_PIN = 2;
const int LAMP_DIGITAL_PIN = 4;
const int PUMP_ON_THRESHOLD = 450; //if MOISTURE_SENSOR value is lower pump won't be turned on
const long int PUMP_TIME_PERIOD = HOURS(12);
const int DELTA_TIME = SECS(1); 

Timer moisture_sensor_timer;

void moisture_sensor_setup()
{
  moisture_sensor_timer.begin(PUMP_TIME_PERIOD);
  pinMode(PUMP_DIGITAL_PIN, OUTPUT);
}

void moisture_sensor_callback()
{
  int val = analogRead(MOISTURE_SENSOR_ANALOG_PIN);
  Serial.print("Wilgoc gleby: ");
  Serial.println(val);
  if(moisture_sensor_timer.available() && val > PUMP_ON_THRESHOLD)
  {
    digitalWrite(PUMP_DIGITAL_PIN, LOW);
    moisture_sensor_timer.restart();
  }
  else
  {
    digitalWrite(PUMP_DIGITAL_PIN, HIGH);
  }
}

void setup() 
{
  Serial.begin(9600);
  moisture_sensor_setup();
  
}

void loop() 
{
  moisture_sensor_callback();
  delay(DELTA_TIME);
}

