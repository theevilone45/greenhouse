# Greenhouse

Automatic watering and lighting of plants

## Dependencies
https://github.com/nettigo/Timers

## TODOS
TODO: Add RTC module\
TODO: Add lamp for plants indoors\
TODO: Add debug/info display module\
TODO: Add DHT11 module\
TODO: Add closed water circuit\

![image](https://drive.google.com/uc?export=view&id=11MywCy2ylORfMzC9pyp_8XQJaxz6LvU7)


